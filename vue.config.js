var path = require('path');

module.exports = {
  // css: {
  //   loaderOptions: {
  //     sass: {
  //       data: `
  //         @import "~bootstrap/scss/functions";
  //         @import "~bootstrap/scss/mixins";
  //         @import "~@/scss/settings/variables.scss";
  //       `,
  //     },
  //   },
  // },
  chainWebpack: (config) => {
    config.module.rules.delete('svg');
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.svg$/,
          loader: 'svg-sprite-loader',
        },
      ],
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
      },
    },
  },
};

module.exports = {
  root: true,
  env: {
    node: true,
  },
  plugins: ['vue-a11y'],
  extends: ['plugin:vue/recommended', '@vue/prettier', 'plugin:vue-a11y/base', 'plugin:prettier/recommended'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue-a11y/click-events-have-key-events': 1,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};

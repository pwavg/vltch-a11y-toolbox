import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/pages/PageHome.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: Home,
    },
  ],
});

import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import store from './store';
import IconAtom from './components/atoms/Icon/IconAtom';

Vue.config.productionTip = false;

// https://tailwindcss.com/docs/installation/#2-add-tailwind-to-your-css
import '@/css/tailwind.scss';

// Global icons
import './assets/icons/icons';
Vue.component('IconAtom', IconAtom);

import VueScrollactive from 'vue-scrollactive';
Vue.use(VueScrollactive);

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount('#app');
